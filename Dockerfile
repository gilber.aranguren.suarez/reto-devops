FROM node:14
WORKDIR /app
COPY package*.json index.js ./
RUN npm install
EXPOSE 5001
CMD ["node", "index.js"]
